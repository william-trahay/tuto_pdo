<!DOCTYPE html>
<html>

<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <title>PDO</title>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
</head>

<body>
  <?php
  // Constantes d'environnement 
  define("DBHOST", "localhost");
  define("DBUSER", "root");
  define("DBPASS", "password");
  define("DBNAME", "tuto_pdo");

  //DSN de connexion
  $dsn = "mysql:dbname=" . DBNAME . ";host=" . DBHOST;

  //Connexion à la base
  try {

    // Instanciation de PDO
    $pdo = new PDO($dsn, DBUSER, DBPASS);

    echo "On est connecté !";

    // Définir le mode de "fetch" par default
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

  } catch (PDOException $e) {
    // On stoppe le script et affiche le message d'erreur
    die("ERREUR !!!:" . $e->getMessage());
  }

  // - à cet instant on est connecté à la base -
  
  // Récupérer des données :
  
  // Préparation de la requête
  $sql = "SELECT * FROM `user`";

  // Execution de la requête
  $requete = $pdo->query($sql);

  // Récupération des données (fetch ou fetchAll)
  // $user = $requete->fetch();
  $user = $requete->fetchAll();

  // Inserer des données 
  $sql = "INSERT INTO `user`(`username`, `password`) VALUES ('Billie', 'billie145')";
  $requete = $pdo->query($sql);
  
  // Modifier des données
  $sql = "UPDATE `user` SET `password` = 'tchoupi87' WHERE `id` = 3";
  $requete = $pdo->query($sql);

  // Supprimer des données
  $sql = "DELETE FROM `user` WHERE `id` > 3";

  $requete = $pdo->query($sql);

  echo "<pre>";
  // var_dump($user);
  var_dump($user[0]["username"]);
  echo "<br/>";
  var_dump($user[1]["id"]);
  echo "<br/>";
  var_dump($user);
  echo "</pre>";

  // Savoir combien de lignes affectées
  echo $requete->rowCount();

  ?>
</body>

</html>